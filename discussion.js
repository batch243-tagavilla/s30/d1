// [Section] MongoDB Aggregation

    /* 
        - used to generate manipulated data and perfrm operations to create filtered results that helps in analyzing data
        - compared to doing CRUD operations, aggregation gives us access to manipulate, filter and compute for results providing us with information to make necessary development decisions without having to create a frontend application
    */

    // Using the aggregate method

        // $match 
        /* 
            - used to pass the documents that meet specified conditions the next pipeline stage/aggregation process 
            Syntax:
                {$match: {field:value}}
        */

        db.fruits.aggregate([
            {
                $match: {onSale:true}
            }
        ]);

        // $group
        /* 
            - used to group elements together and field value pairs, using the data from the grouped elements
            Syntax:
                {$group: {_id:"value", fieldResult:"valueResult"}}
        */
    
        db.fruits.aggregate([
            {
                $group: {
                    _id:"$supplier_id", 
                    total:{$sum:"$stock"}
                }
            }
        ]);

    // Aggregating Documents using 2 pipeline stages
        /* 
            Syntax:
                db.collectionName.aggregrate([
                    {
                        $match: {field:value}
                    },
                    {
                        $group: {
                            _id:"value",
                            fieldResult:"valueResult"
                        }
                    }

                ])
        */

        db.fruits.aggregate([
            {
                $match: {onSale:true}
            },
            {
                $group: {
                    _id:"$supplier_id", 
                    total:{$sum:"$stock"}
                }
            }
        ]);

    // Field Projection with aggregation

        // $project
        /* 
            - can be used when aggregating data to include/exclude fields from the returned results
            - the field that is set to zero(0) or false will be excluded
            - the field that is set to one(1) or true will be included
            Syntax:
                {
                    $project: {
                        field:1/0
                    }
                }
        */

        db.fruits.aggregate([
            {
                $project: {
                    _id: false
                }
            }
        ]);

        db.fruits.aggregate([
            {
                $match: {onSale:true}
            },
            {
                $group: {
                    _id: "$supplier_id", 
                    total: {$sum:"$stock"}
                }
            },
            {
                $project: {
                    _id: false
                }
            }
        ]);

    // Sorting aggregated results

        // sort
        /* 
            - used to change the order of the aggregated results
            - providing a value of -1 will sort the aggregated reulsts in a reverse order
            Syntax:
                {
                    $sort: {
                        field: 1/-1
                    }
                }
        */

        db.fruits.aggregate([
            {
                $match: {onSale:true}
            },
            {
                $group: {
                    _id: "$supplier_id", 
                    total: {$sum:"$stock"}
                }
            },
            {
                $sort: {
                    total: -1
                }
            }
        ]);

    // Aggregating results based on the array fields

        // $unwind
        /* 
            - deconstructs an array fields from a collection with an array value to give us a result for each array elements
            Syntax: 
                {$unwind:field}
        */

        db.fruits.aggregate([
            {
                $unwind:"$origin"
            }
        ]);

        db.fruits.aggregate([
            {
                $unwind:"$origin"
            },
            {
                $group: {
                    _id: "$origin", 
                    kinds: {$sum:1}
                }
            }
        ]);